
module.exports = {
  appUrl: 'http://localhost:5000',
  launchOptions: {
    headless: true,
    devtools: false,
    defaultViewport: {
      width: 0,
      height: 0
    }
  }
};
